FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

RUN apt-get update -y && apt-get install -y

CMD ["npm", "run", "start"]
