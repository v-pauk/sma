An implemented endpoint for calculation Simple Moving Average.

```
    GET {apiDomain}/api/v1/rate/{period}
```

`{period}` should be integer value (7, 25, 99).

#### Request example
```
    GET {apiDomain}/api/v1/rate/7
```

#### Response example 
```
    {
        "data": {
            "value": 52409.563
        }
    }
```

#### Test environment
http://sma.pauk.space/api/v1/rate/7

#### Run locally with docker
1. docker-compose build
2. docker-compose up -d
3. Open application on 3000 port: http://localhost:3000/api/v1/rate/{period}
