import * as WebSocket from 'ws';
import {injectable} from 'tsyringe';
import DataService from '../service/data.service';

@injectable()
export default class WebSocketServer {
    private static readonly API_ADDRESS = 'wss://api-pub.bitfinex.com/ws/2';
    private readonly webSocket: WebSocket;

    public constructor(private dataService: DataService) {
        this.webSocket = new WebSocket(WebSocketServer.API_ADDRESS);
        this.onMessage();
        this.onOpen();
    }

    private onMessage(): void {
        this.webSocket.on('message', (message) => {
            this.handleMessage(message);
        })
    }

    private onOpen(): void {
        const message = JSON.stringify({
            event: 'subscribe',
            channel: 'ticker',
            symbol: 'tBTCUSD'
        });

        this.webSocket.on('open', () => this.webSocket.send(message));
    }

    private handleMessage(message: any): void {
        const data = JSON.parse(message as string);

        console.log('Received message: ', message);

        if (typeof data === 'object' && Array.isArray(data)) {
            if (typeof data[1] === 'object' && Array.isArray(data[1])) {
                const btcRate = data[1][6];
                this.dataService.push(btcRate);

                console.log('BTC rate: ', btcRate);
            }
        }
    }
}
