import 'reflect-metadata';
import {createExpressServer, useContainer} from 'routing-controllers';
import {RateController} from './controller/rate.controller';
import {TsyringeAdapter} from './core/di/tsyringe.adapter';
import WebSocketServer from './websocket/websocket.server';
import {container} from 'tsyringe';
import {ErrorHandler} from './middleware/error.handler';

const tsyringeAdapter = new TsyringeAdapter();
useContainer(tsyringeAdapter);

const app = createExpressServer({
    routePrefix: '/api/v1',
    middlewares: [ErrorHandler],
    defaultErrorHandler: false,
    controllers: [RateController],
});

app.listen(3000);

container.resolve(WebSocketServer);
