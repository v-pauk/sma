export class DataView {
    private constructor(public data: object) {
    }

    public static create(data: object): DataView {
        return new DataView(data);
    }
}
