export class LastSmaView {
    private constructor(public value: number) {
    }

    public static create(value: number): LastSmaView {
        return new LastSmaView(value);
    }
}
