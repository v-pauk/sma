import {singleton} from 'tsyringe';

@singleton()
export default class DataService {
    private data: number[] = [];

    public push(value: number): void {
        this.data.push(value);
    }

    public all(): number[] {
        return this.data;
    }
}
