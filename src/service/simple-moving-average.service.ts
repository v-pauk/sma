import {injectable} from 'tsyringe';
import DataService from './data.service';
import {BadRequestError} from 'routing-controllers';

@injectable()
export default class SimpleMovingAverageService {
    public constructor(private dataService: DataService) {
    }

    public calculate(period: number): number {
        const list = [...this.dataService.all()];
        console.log(list);
        if (list.length < period) {
            throw new BadRequestError(`Not enough data for the period: ${period}`)
        }

        const start = list.length - period;
        const range = list.slice(start, list.length);
        console.log(range);
        const sum = range.reduce((a, b) => a + b, 0);
        const average = sum / period;

        return average;
    }
}
