import {IsInt, Min} from 'class-validator';

export default class RateDTO {
    @Min(1)
    @IsInt()
    public period: number;
}
