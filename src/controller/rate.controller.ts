import {JsonController, Get, Params} from 'routing-controllers';
import {injectable} from 'tsyringe';
import SimpleMovingAverageService from '../service/simple-moving-average.service';
import {LastSmaView} from '../response/view/last-sma.view';
import {DataView} from '../response/view/data.view';
import RateDTO from '../request/dto/rate.dto';

@injectable()
@JsonController('/rate')
export class RateController {

    public constructor(private smaService: SimpleMovingAverageService) {
    }

    @Get('/:period')
    public get(@Params() rateDTO: RateDTO) {
        const result = this.smaService.calculate(rateDTO.period);

        return DataView.create(LastSmaView.create(result));
    }
}
