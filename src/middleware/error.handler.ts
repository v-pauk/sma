import {Middleware, ExpressErrorMiddlewareInterface, BadRequestError} from 'routing-controllers';
import {NextFunction, Response, Request} from 'express';
import {ValidationError} from 'class-validator';

@Middleware({ type: 'after' })
export class ErrorHandler implements ExpressErrorMiddlewareInterface {
    private static readonly INTERNAL_ERROR_STATUS = 500;

    public error(error: any, request: Request, response: Response, next: (error: any) => NextFunction) {
        console.log(error);
        if (error instanceof BadRequestError) {
            let message: string;
            if (error.hasOwnProperty('errors')) {
                message = this.formatValidationErrors((error as any).errors)
            } else {
                message = error.message;
            }

            return response.status(error.httpCode).json({message});
        } else {
            return response.status(ErrorHandler.INTERNAL_ERROR_STATUS).json({message: 'Internal server error'});
        }
    }

    private formatValidationErrors(errors: ValidationError[]): string {
        const errorMessages: string[] = [];
        for (const item of errors) {
            for (const constraint in item.constraints) {
                if (item.constraints[constraint]) {
                    errorMessages.push(item.constraints[constraint]);
                }
            }
        }

        return errorMessages.join(', ');
    }
}
