import { IocAdapter, Action, ClassConstructor } from 'routing-controllers';
import {container} from 'tsyringe';

export class TsyringeAdapter implements IocAdapter {
    public get<T>(someClass: ClassConstructor<T>, action?: Action): T {
        const childContainer = container.createChildContainer();
        return childContainer.resolve<T>(someClass);
    }
}
